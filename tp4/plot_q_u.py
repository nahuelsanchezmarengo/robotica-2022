import matplotlib.pyplot as plt

def plot_q_u(q, u, ts):
    fig = plt.figure(figsize=(10,10))
    plt.subplot(3, 1, 1), plt.plot(ts, q[0, :], ), plt.grid(True)
    plt.ylabel(r'$q_1 (\theta)$ [rad]'), #plt.xlabel('t [s]')
    plt.subplot(3, 1, 2), plt.plot(ts, q[1, :]), plt.grid(True)
    plt.ylabel(r'$q_2 (\omega)$ [rad/s]'), #plt.xlabel('t [s]')
    plt.subplot(3, 1, 3), plt.plot(ts, u[0, :]), plt.grid(True)
    plt.ylabel('$u$'), plt.xlabel('t [s]')
    plt.show()
