import numpy as np
import matplotlib.pyplot as plt

from plot_q_u import plot_q_u

# Parámetros de simulación.
J = 100e-3      # Momento de inercia
T = 10          # Tiempo de simulación
dt = 0.1        # Intervalo de muestreo
N = int(T/dt)   # Índice máximo de estados discretos

# Vector de tiempos discretos
ts = np.linspace(0, T-dt, N)

# Vector de estado inicial.
# q1 = theta; q2 = theta_punto
q0 = np.array( (0, 1) )

# Vector de estado e inicialización.
q = np.zeros( (2, N) )
q[:, 0] = q0

# Vector de acciones de control.
u = np.zeros( (1, N) )
#u = np.ones( (1, N) )

# Matrices del sistema de estado discretizado.
A = np.array([ [1, dt], [0, 1] ])
B = np.array([ [0], [dt/J] ])

# Bucle para el cálculo de los estados.
for i in range(1, N):
    q[:, i] = A.dot( q[:, i-1] ) + B.dot( u[:, i-1] )


plot_q_u(q, u, ts)
# fig = plt.figure()
# plt.subplot(3, 1, 1), plt.plot(ts, q[0, :], ), plt.grid(True)
# plt.ylabel(r'$q_1 (\theta)$ [rad]'), #plt.xlabel('t [s]')
# plt.subplot(3, 1, 2), plt.plot(ts, q[1, :]), plt.grid(True)
# plt.ylabel(r'$q_2 (\omega)$ [rad/s]'), #plt.xlabel('t [s]')
# plt.subplot(3, 1, 3), plt.plot(ts, u[0, :]), plt.grid(True)
# plt.ylabel('$u$'), plt.xlabel('t [s]')
# plt.show()
